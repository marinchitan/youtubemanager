-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2018 at 07:03 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youtubemanager_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` text,
  `surname` text,
  `email` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `surname`, `email`, `password`) VALUES
(1, 'Jane', 'Doe', 'test1@example.com', 'test1'),
(2, 'Rob', 'Peterson', 'test2@example.com', 'test2'),
(3, 'Kevin', 'Clarks', 'test3@example.com', 'test3'),
(12, 'T', 'T', 'TT', 'TT'),
(13, NULL, NULL, '', ''),
(14, 'T', 'T', 'TTTT', 'TTTT'),
(19, 'S', 'S', 'S', 'S'),
(25, 'SS', 'SS', 'SS', 'SS'),
(33, 'SSS', 'SSS', 'SSS', 'SSS');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `video_id` text NOT NULL,
  `url` text NOT NULL,
  `name` text,
  `rating` int(11) DEFAULT NULL,
  `tags` text,
  `groups` text,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`video_id`, `url`, `name`, `rating`, `tags`, `groups`, `user_id`) VALUES
('GLBXlhktt7M', 'https://www.youtube.com/watch?v=GLBXlhktt7M', 'Vanill Wow #2', 3, 'Warcraft,Wow', 'Gaming', 2),
('je6NuyxePnQ', 'https://www.youtube.com/watch?v=je6NuyxePnQ', 'XXYYXX - I Don\'t feat. $k', 5, 'Music/SK/XXYYXX', 'Music', 2),
('Kbg777rhMrA', 'https://www.youtube.com/watch?v=Kbg777rhMrA', 'Liverpool vs Machester City, 4:3', 4, 'Football,Liverpool', 'Sport', 2),
('KrxJQAYdfXo', 'https://www.youtube.com/watch?v=KrxJQAYdfXo', 'XXYYXX - Secrets', 5, 'Music/XXYYXX', 'Music', 1),
('KXCISSt_DKg', 'https://www.youtube.com/watch?v=KXCISSt_DKg', 'Drawing with Peter: Let\'s Get Started on a Doodle', 3, 'Draw,Peter', 'Entertainment', 2),
('PMd9eYXT0mM', 'https://www.youtube.com/watch?v=PMd9eYXT0mM', 'XXYYXX - Alone', 4, 'Music/XXYYXX/Alone', 'Music', 3),
('RevirDl5oAU', 'https://www.youtube.com/watch?v=RevirDl5oAU', 'Video : [RevirDl5oAU]', 4, 'Wow', 'Gaming', 2),
('uf9WLPZK2AU', 'https://www.youtube.com/watch?v=uf9WLPZK2AU', 'XXYYXX - DMT', 4, 'Music/Chill/XXYYXX', 'Music', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `unique_email` (`email`(50));

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`(60)),
  ADD UNIQUE KEY `unique_url` (`url`(200)),
  ADD KEY `video_fk` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `video_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
