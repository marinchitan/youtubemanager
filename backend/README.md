# YoutubeManager

[![node](https://img.shields.io/node/v/passport.svg)]()
[![MyGet](https://img.shields.io/myget/mongodb/v/MongoDB.Driver.Core.svg)]()


## What is YoutubeManager?
__YoutubeManager__ is user friendly online application for managing your favorite youtube videos.

## What are the features of YoutubeManager?

__YoutubeManager__ offers a large specter of featrues that enriches the usual YouTube Experience


1. **Adding Tags to videos**

	Personalize your video playlist by adding custom tags for your video which
	you can use to seach for yout videos.
	
2. **Video Grouping**

	Create custom groups of videos within a playlist.
	
3. **Video Rating**

	Use a 1-5 stars system to give custom rates to your videos add sort them using
	their ratings.
	
4. **Account safe system**
	
	Access your favorite video within any device and any browser with ease using
	your account.
	
5. **Embedded download**
	
	Download your videos with ease using our embedded download function.
	
__(c) Chitan Marin. Bucharest 2017. Academy of Economic Studies__
