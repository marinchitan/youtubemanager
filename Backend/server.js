const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const path = require('path');

const sequelize = new Sequelize(
	'youtubemanager_db', 
	'root', 
	'',
	{
    	dialect:"mysql"
	},
	{
		define:{timestamps:false}
	}
);

//App
const port = 3000;
const app = express();
const cors = require('cors');

app.use(cors())
//Sequlize Models
const User = sequelize.define('users', {
	user_id: {type:Sequelize.INTEGER, primaryKey:true, autoIncrement:true},
	name: Sequelize.STRING,
	surname: Sequelize.STRING,
	email: Sequelize.STRING,
	password: Sequelize.STRING
	},
	{
		timestamps:false
	}
);

const Video = sequelize.define('videos', {
	video_id: {type:Sequelize.STRING, primaryKey:true},
	url: Sequelize.STRING,
	name: Sequelize.STRING,
	rating: Sequelize.INTEGER,
	tags: Sequelize.STRING,
	groups: Sequelize.STRING,
	user_id: Sequelize.INTEGER
	},
	{
		timestamps:false
	}
)

//Middleware
//app.use(express.static(__dirname + '/Views'));
app.use(bodyParser.json());
app.use(express.static('../frontend/build'))

//Routing

//API Test
/*app.get("/", (req, res) => {
	res.status(201).render('index.html');
});*/

app.get("/test", (req, res)=> {
	res.status(201).send("Succesfull test...")
})

app.get("/sync", (req, res) => {
	sequelize.sync()
	.then(() => res.status(201).send('Succesfull sync...'))
	.catch((err) => {console.warn(err)
					res.status(500).send('Sync error.')
				})
});

app.get("/users", (req, res) => {
	User.findAll({})
	.then((users) => res.status(202).json(users))
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve users')
	})
});

app.get("/videos", (req, res) => {
	Video.findAll({})
	.then((videos) => res.status(202).json(videos))
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve videos')
	})
});

app.get("/videos/:id", (req, res) => {
	Video.findById(req.params.id)
	.then((video) => {
		if(video){
			res.status(202).json(video)
		}else {
			res.status(404).send('Video not found..')
		}
	})
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve the required video')
	})
});

//Add routing for getting videos by criteria
app.get("/videos-bygroup/:group", (req,res) => {
	Video.findAll({where:{groups:req.params.group}})
	.then((videos)=>res.status(202).json(videos))
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve videos')
	})
})

app.get("/videos-byid/:id", (req,res) => {
	Video.findAll({where:{video_id:req.params.id}})
	.then((videos)=>res.status(202).json(videos))
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve videos')
	})
})

app.get("/videos-bytags/:tags", (req,res) => {
	Video.findAll({where:{tags:req.params.tags}})
	.then((videos)=>res.status(202).json(videos))
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve videos')
	})
})

app.get("/users/:id", (req, res) => {
	User.findById(req.params.id)
	.then((user) => {
		if(user){
			res.status(202).json(user)
		}else {
			res.status(404).send('User not found..')
		}
	})
	.catch((err) => {
		console.warn(err)
		res.status(500).send('Error, can not retrieve the required user')
	})
});

app.post("/users", (req, res) => {
	User.create(req.body)
	.then(() => res.status(204).send("Succesfully created user.."))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not create user..")
	} )
})

app.post("/videos", (req, res) => {
	Video.create(req.body)
	.then(() => res.status(204).send("Succesfully created video.."))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not create user..")
	} )
})

app.put("/videos/:id", (req, res) => {
	Video.findById(req.params.id)
	.then((video) => {
		if(video){
	       return video.update(req.body) 
		}else {
			res.status(404).send("Video not found")

		}
	})
	.then(() => res.status(202).send("Video updated"))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not update video")
	})
})


app.put("/users/:id", (req, res) => {
	User.findById(req.params.id)
	.then((user) => {
		if(user){
	       return user.update(req.body) 
		}else {
			res.status(404).send("User not found")

		}
	})
	.then(() => res.status(202).send("User updated"))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not update user")
	})
})

app.delete("/users/:id", (req, res) => {
	User.findById(req.params.id)
	.then((user) => {
		if(user){
			return user.destroy()
		}else {
			res.status(404).send("User not found")
		}
	})
	.then(() => res.status(202).send("User deleted"))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not delete user")
	})
})


app.delete("/videos/:id", (req, res) => {
	Video.findById(req.params.id)
	.then((video) => {
		if(video){
			return video.destroy()
		}else {
			res.status(404).send("Video not found")
		}
	})
	.then(() => res.status(202).send("Video deleted"))
	.catch((err) => {
		console.warn(err)
		res.status(501).send("Can not delete video")
	})
})

//Run
console.log("Server running on ["+port+"]...")
app.listen(port);