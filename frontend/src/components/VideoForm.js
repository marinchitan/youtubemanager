import React, { Component } from 'react';
import '../styles/w3.css';
import '../styles/videoform.css';
import VideoStore from '../stores/VideoStore'
import { EventEmitter} from 'fbemitter'
import Video from './Video'
import axios from 'axios'

const ee = new EventEmitter()
const store = new VideoStore(ee)

function addVideo(video){
	store.addOne(video);
	window.location.reload(true);
}

function getNameOfVideo(id){
	var url = 'https://www.googleapis.com/youtube/v3/videos?id='+id.toString()+'&key=AIzaSyDpVV0wvLBUIqOP_LI8bseT_ZtEZkKWPXE&fields=items(snippet(title))&part=snippet' 
	var output;
	axios(url)
		.then((response) => {
			console.log(url);
			console.log(response['data']['items'][0]['snippet']['title']);
			this.state.videoName = response['data']['items'][0]['snippet']['title'];
			
		})
		.catch((error) => console.warn(error))
	
}



class VideoForm extends Component{
	constructor(props){
		super(props)
		this.state = {
			videoId:"",
			videoUrl:"https://www.youtube.com/watch?v=",
			videoRating:4,
			videoName:"",
			videoGroup:"Gaming",
			videoTags:""
		}
		this.handleChange = (event) => {
      		this.setState({
        		[event.target.name] : event.target.value
     	    })
    	}
    	this.handleSelectChange = (event) => {
      		this.setState({
        		[event.target.name] : event.target.value
     	    })
    	}
    	this.getNameOfVid = function(id){
    		var url = 'https://www.googleapis.com/youtube/v3/videos?id='+id.toString()+'&key=AIzaSyDpVV0wvLBUIqOP_LI8bseT_ZtEZkKWPXE&fields=items(snippet(title))&part=snippet' 
			var output;
			axios(url)
			.then((response) => {
			//console.log(url);
			console.log(response['data']['items'][0]['snippet']['title']);
			return response['data']['items'][0]['snippet']['title'];
			
		})
		.catch((error) => console.warn(error))
    	}
	}

	render(){
		return(
			<div>
				<div className="addVideoForm">
					<div className="w3-bar">					
							<input name="videoId" id="inp-id" className="w3-bar-item w3-input w3-border" placeholder="Supply Video ID" type="text"
							onChange={this.handleChange}></input>
							<input name="videoTags" id="inp-tags" className="w3-bar-item w3-input w3-border" placeholder="Supply Video Tags" type="text"
							onChange={this.handleChange}></input>
							<select ref = {(input)=> this.menu = input} name="videoGroup" id="select-group" className="w3-select w3-border" name="group_option"
							onChange={this.handleChange}>
	    						<option value="" disabled selected>Choose group</option>
	     						<option value="Music">Music</option>
	     						<option value="Sport">Sport</option>
	    						<option value="Gaming">Gaming</option>
	   							<option value="Entertaiment">Entertainment</option>
	  						</select>
							<select ref = {(input)=> this.menu = input} name="videoRating" id="select-rating" className="w3-select w3-border" name="rating_option"
							onChange={this.handleChange}>
	    						<option value="" disabled selected>Pick a rating</option>
	     						<option value="1">1</option>
	     						<option value="2">2</option>
	    						<option value="3">3</option>
	   							<option value="4">4</option>
	   							<option value="5">5</option>
	  						</select>
							<button id="add-btn" className="w3-bar-item w3-button w3-red"
							onClick= { () => {
								//Try to resolve the problem with API
								//this.state.videoName = this.getNameOfVid(this.state.videoId);

								addVideo({
								video_id:this.state.videoId,
								url:this.state.videoUrl+this.state.videoId,
								name:"Video : ["+this.state.videoId+"]",
								rating:this.state.videoRating,
								groups:this.state.videoGroup,
								tags:this.state.videoTags,
								user_id:2
								})

								

								}
							}
							>Add</button>						
					</div>
				</div>
			</div>

		);
	}
}

export default VideoForm;
