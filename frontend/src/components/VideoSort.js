import React, { Component } from 'react';
import '../styles/w3.css';
import '../styles/videosort.css';
import axios from 'axios'
import VideoStore from '../stores/VideoStore'
import { EventEmitter} from 'fbemitter'

const ee = new EventEmitter();
	const store = new VideoStore(ee);

	function loadByGroups(group){
		store.getAllByGroup(group);
		window.location.reload(true);
	}

	function loadTest(){
		window.location.reload(true);
		store.getAllByGroup('Music');
	}
class VideoSort extends Component{
	
	constructor(props){
		super(props)
		this.state = {
			groupsel:"Gaming",
			ratingsel:4,
			tagssel:"Wow"
		}
		this.handleChange = (event) => {
      		this.setState({
        		[event.target.name] : event.target.value
     	    })
    	}
    }


	render(){
		return(
			<div>
			<div className="sortVideo">
				<div className="w3-bar">
					<button id="all-sort" className="w3-bar-item w3-button w3-gray">All</button>
					<button id="rating-sort" className="w3-bar-item w3-button w3-red">Tags</button>
					<button onClick = "()=>{console.log('something')}" id="group-sort" className="w3-bar-item w3-button w3-red">Rating</button>
					<button id="tags-sort" onClick = {()=>{loadTest()}} className="w3-bar-item w3-button w3-red">Group</button>
				</div>
			</div>

			<div className="sortVideo">
				<div className="w3-bar">
					<button id="placeholder-sort" className="w3-bar-item w3-button w3-red"></button>
					<select ref = {(input)=> this.menu = input} onChange={this.handleChange} name="ratingsel" id="sort-rating" className="w3-select w3-border" name="rating_option">
	    						<option value="1" selected>1</option>						
	     						<option value="2">2</option>
	    						<option value="3">3</option>
	   							<option value="4">4</option>
	   							<option value="5">5</option>
	  				</select>
					<select ref = {(input)=> this.menu = input} onChange={this.handleChange} name="groupsel" id="sort-group" className="w3-select w3-border" name="group_option">
	    						<option value="music" selected>Music</option>
	     						<option value="sport">Sport</option>
	    						<option value="gaming">Gaming</option>
	   							<option value="entert">Entertainment</option>
	  				</select>
	  				<input name="tagssel" onChange={this.handleChange} id="sort-tags" className="w3-bar-item w3-input w3-border" placeholder="Tag1,Tag2..." type="text"></input>
				</div>
			</div>
			</div>
		);
	}
}

export default VideoSort;