import React, { Component } from 'react';
import '../styles/App.css';
import Header from './Header'
import VideoGrid from './VideoGrid'

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <VideoGrid/>
      </div>
    );
  }
}

export default App;
