import React, { Component } from 'react'
import '../styles/video.css';

class Video extends Component{
	constructor(props){
		super(props)
		this.state = {
			video: this.props.video,
			videoId: this.props.video.video_id,
			videoUrl: this.props.video.url,
			videoName: this.props.video.name,
			videoGroup: this.props.video.groups,
			videoRating: this.props.video.rating,
			videoTags: this.props.video.tags,
			videoEmbedUrl: "https://www.youtube.com/embed/"+this.props.video.video_id
			}		
		}

		componenWillReceiveprops(nextProps){
			this.setState({
			video:nextProps,
			videoId: this.props.video.video_id,
			videoUrl: this.props.video.url,
			videoName: this.props.video.name,
			videoGroup: this.props.video.groups,
			videoRating: this.props.video.rating,
			videoTags: this.props.video.tags,
			videoEmbedUrl: "https://www.youtube.com/embed/"+this.props.video.video_id
			})
		}

		render(){
			return(
				<div>
					<div id="video-grid">

						<div id="video-box">
							<iframe id="video-frame" width="420" height="345" src={this.state.videoEmbedUrl}>
							</iframe>
						</div>

						<div id="car1-box">
							<div id="op-name">{this.state.videoName}</div>
							<button id="op-delete" className="w3-button w3-xlarge w3-red w3-card-4" onClick = { () => this.props.onDelete(this.state.videoId)}>✕</button>
						</div>

						<div id="car2-box">
							<div id="op-group">Group: {this.state.videoGroup}</div>
							<div id="op-rating">Rating: {this.state.videoRating}</div>
							<div id="op-tags">Tags: {this.state.videoTags}</div>
						</div>	

					</div>
				</div>

			);
		}
	}

export default Video