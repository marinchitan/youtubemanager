import React, { Component } from 'react'
import VideoStore from '../stores/VideoStore'
import { EventEmitter} from 'fbemitter'
import Video from './Video'

const ee = new EventEmitter()
const store = new VideoStore(ee)

function addVideo(video){
	store.addOne(video);
}

function deleteVideo(id){
	store.deleteOne(id);
	window.location.reload(true);

}


//Change flag for sorting in componentDidMount on reload !
var flag = 'all'

class VideoGrid extends Component{

	constructor(props){
		super(props)
		this.state = {
			videos : [],
			selected : null
		}
		this.loadTest = function(){	
			flag = 'group'
			window.location.reload(true);
			console.log(flag);
		}
		
	}
	componentDidMount(){
		if(flag == 'all')
		{
			store.getAll();
			ee.addListener('VIDEOS_ALL_LOAD', () => {
				this.setState({
					videos: store.content
				})			
			})
		}else if(flag == 'group'){
			store.getAllByGroup('Music');
			ee.addListener('VIDEOS_ALL_LOAD_GROUP', () => {
				this.setState({
					videos: store.content
				})			
			})
		}
	}
	

	render(){
		return(
			<div>		
				{this.state.videos.map((object,i)=> <Video video={object} key={i} onDelete={deleteVideo}/>)}
					
			</div>

		);
	}
}

export default VideoGrid