import React, { Component } from 'react';
import '../styles/w3.css';
import '../styles/header.css';
import VideoForm from './VideoForm'
import VideoSort from './VideoSort'


class Header extends Component{
	render(){
		return(
			<div>
				<div className="header-text">Add Video:</div>
				<VideoForm/>
				<div className="header-text">Sort Videos By:</div>
				<VideoSort/>

			</div>
		);
	}
}

export default Header;