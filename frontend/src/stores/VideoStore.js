import axios from 'axios'
const SERVER = 'http://localhost:3000'

class VideoStore{
	constructor(ee){
		this.ee = ee;
		this.content = [];
		this.selected = null;
	} 
	getAll(){
		axios(SERVER + '/videos')
		.then((response) => {
			this.content = response.data
			this.ee.emit('VIDEOS_ALL_LOAD')
		})
		.catch((error) => console.warn(error))
	}
	getAllByGroup(group){
		axios(SERVER+'/videos-bygroup/'+group)
		.then((response) => {
			this.content = response.data
			this.ee.emit('VIDEOS_ALL_LOAD_GROUP')
		})
		.catch((error) => console.warn(error))
	}
	addOne(video){
		axios.post(SERVER + '/videos', video)
		.then(()=>this.getAll())
		.catch((error) => console.warn(error))
	}
	deleteOne(id){
		axios.delete(SERVER + '/videos/' + id)
		.then(()=>this.getAll())
		.catch((error) => console.warn(error))
	}
	saveOne(id, video){
		axios.put(SERVER+ '/videos/'+id, video)
		.then(()=>this.getAll())
		.catch((error) => console.warn(error))
	}
}

export default VideoStore